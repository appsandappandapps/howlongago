import SwiftUI
import shared

struct TaskDetail: View {

    @Environment(\.scenePhase) var scenePhase
    
    @EnvironmentObject var _taskDetailsObserver: ObservableTaskDetails
    @EnvironmentObject var navigationObserver: ObservableNavigation
    var taskDetailsUIState: TaskDetailsUIState { get { return _taskDetailsObserver.uiState } }
    var taskDetailsUIEvents: TaskDetailsUIEvents { get { return _taskDetailsObserver.uiEvents } }
    @State var deletingInstanceUid: Int64 = 0
    @State var tappedOnView: TimeInterval = Double(0)
    @State var isPopupPresented: Bool = false
    
    @State var popupFrame: CGRect? = nil
    @State var editingInstance: InstanceUI? = nil
    
    @State var date: Date = Date()
    @State var note: String = ""
    
    var taskId: Int64 {
        get { navigationObserver.uiState.taskIdForDetailsPage }
    }
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
            }
            Button(
                action: { taskDetailsUIEvents.onInsertInstanceAndExpandIt(taskId: taskId) },
                label: {
                    Text("I just did it")
                }
            )
            List(taskDetailsUIState.instances, id: \.uid) { instance in
                InstanceRow(
                    instance: instance,
                    popupFrameLocation: $popupFrame,
                    editingInstance: $editingInstance,
                    isLast : taskDetailsUIState.instances.firstIndex(of: instance) == taskDetailsUIState.instances.count - 1
                )
                .environmentObject(_taskDetailsObserver)
                .onTapGesture {
                    print("list item clicked")
                }
            }
        }
        .padding(0)
        .onChange(of: scenePhase) { phase in
          if phase == .active {
              taskDetailsUIEvents.onForceUIUpdate()
          }
        }
        .toolbar {
            ToolbarItem(placement: .keyboard) {
                HStack {
                    Spacer()
                    Button("Dismiss") {
                        UIApplication.shared.endEditing()
                    }
                    .font(.system(size: 16.0))
                }
            }
        }
        .coordinateSpace(name: "custom")
        .overlay(alignment: .topLeading) {
            if let pf = popupFrame {
                InstanceEditPopup(
                    onClose: { popupFrame = nil },
                    popupFrame: pf,
                    instance: $editingInstance
                )
                .environmentObject(_taskDetailsObserver)
                .transition(.moveInAndOut)
            }
        }
        .animation(.spring(), value: popupFrame)
        .onTapGesture {
            tappedOnView = Date().timeIntervalSince1970
        }
    }
}

