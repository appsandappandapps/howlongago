import Foundation
import SwiftUI
import shared

struct Settings: View {
    @EnvironmentObject var navigationObserver: ObservableNavigation
    @EnvironmentObject var tasksObserver: ObservableTasks
    var tasksUIState: TasksUIState { get { tasksObserver.uiState } }
    let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    
    @State var presentFileImport: Bool = false
    @State var presentDbSave: Bool = false
    @State var fileContent: String = ""
    @State var stringDatabase: String = ""
    @State var backedup: Bool = false
    
    var selectedSort: Binding<Int> { Binding(
        get: {
            if(tasksUIState.sortType == .dateDesc) {
                return 0
            } else if tasksObserver.uiState.sortType == .dateAsc {
                return 1
            } else if tasksObserver.uiState.sortType == .nameAsc {
                return 2
            } else if tasksObserver.uiState.sortType == .nameDesc {
                return 3
            } else {
                return 3
            }
        },
        set: { num in
            if num == 0 {
                tasksObserver.uiEvents.onClickSortItem(sort: .dateDesc)
            } else if num == 1 {
                tasksObserver.uiEvents.onClickSortItem(sort: .dateAsc)
            } else if num == 2 {
                tasksObserver.uiEvents.onClickSortItem(sort: .nameAsc)
            } else if num == 3 {
                tasksObserver.uiEvents.onClickSortItem(sort: .nameDesc)
            }
        }
    ) }
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text("Settings") .font(.largeTitle) .fontWeight(.bold)
                Spacer()
                Button { navigationObserver.uiEvents.onToggleSettings() } label: {
                  Image(systemName: "xmark.circle")
                }
            }.padding(.all, 15.0)
            Form {
                Section(header: Text("Sorting")) {
                    Picker("", selection: selectedSort, content: {
                        Text("Most recent").tag(0)
                        Text("Least recent").tag(1)
                        Text("Alphabetical").tag(2)
                        Text("Reverse alphabetical").tag(3)
                    })
                    .pickerStyle(.menu)
                    .labelsHidden()
                }
                Section(header: Text("App details")) {
                    VStack(alignment: .leading) {
                        Text("Version \(String(appVersion!))")
                            .padding(.leading, 4.0)
                            .padding(.top, 5.0)
                        Link("☞ Source code",
                              destination: URL(string: "https://gitlab.com/appsandappsandapps/howlongago")!)
                            .buttonStyle(.borderless)
                            .padding(.top, 0)
                            .padding(.all, 5)
                            .padding(.leading, 0)
                            .padding(.bottom, 0)
                            .padding(.top, 0)
                        Link("☞ App Store",
                              destination: URL(string: "https://gitlab.com/appsandappsandapps/")!)
                            .buttonStyle(.borderless)
                            .padding(.leading, 0)
                            .padding(.all, 5)
                            .padding(.bottom, 5)
                    }
                    .foregroundColor(.black)
                    .font(.system(size: 14.0))
                }
                Section(header: Text("Import and backup")) {
                    VStack(alignment: .leading) {
                        Button("Import new tasks") {
                            presentFileImport = true
                        }
                        .padding(.top, 5)
                        .padding(.bottom, 1)
                        .buttonStyle(.borderless)
                        Text("☞ Importing doesn't remove your existing tasks")
                            .font(.system(size: 14.0))
                            .padding(.bottom, 5)
                        HStack {
                            Button("Backup to filesystem") {
                                let backupImport: BackupImportUseCase = ServiceLocator().backupImportUseCase!
                                backupImport.exportDatabase() { stringDatabase, b in
                                    self.stringDatabase = stringDatabase!
                                    presentDbSave = true
                                }
                            }
                            if backedup {
                                Text("✔︎")
                                    .font(.system(size: 18.0))
                                    .foregroundColor(.blue)
                                    .transition(.opacity)
                            } else {
                                Text(" ")
                                    .font(.system(size: 18.0))
                            }
                        }
                        .padding(.bottom, 1)
                        .buttonStyle(.borderless)
                        Text("☞ You can backup to iCloud, Google Drive etc")
                            .font(.system(size: 14.0))
                            .padding(.bottom, 1)
                    }
                    .animation(.easeInOut, value: backedup)
                }
            }
            Spacer()
        }.sheet(isPresented: $presentFileImport) {
            DocumentPicker(fileContent: $fileContent)
        }.sheet(isPresented: $presentDbSave) {
            DocumentPickerSave(toSave: $stringDatabase, backedup: $backedup)
        }
        .onChange(of: fileContent) { file in
            let backupUseCase: BackupImportUseCase = ServiceLocator().backupImportUseCase!
            backupUseCase.importDatabase(stringDb: file) { error in
                if error == nil {
                    navigationObserver.uiEvents.onToggleSettings()
                }
            }
        }
    }
}
