package app.howlongago.ui.comps.instance

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.relocation.BringIntoViewRequester
import androidx.compose.foundation.relocation.bringIntoViewRequester
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.howlongago.clickableNoRipple
import app.howlongago.taskdetails.model.InstanceUI
import app.howlongago.ui.comps.LocalTaskDetailsUIEvents
import app.howlongago.ui.comps.task.InstanceNotesTextFieldTextStyle
import app.howlongago.ui.comps.task.InstanceNotesUpArrowTextStyle
import kotlinx.coroutines.launch

// And edit text that allows us to
// edit the instance note
@OptIn(
    ExperimentalComposeUiApi::class,
    ExperimentalAnimationApi::class,
    ExperimentalFoundationApi::class
)
@Composable
fun InstanceNoteEdit(
    modifier: Modifier = Modifier,
    instance: InstanceUI,
    onNoteEdit: (Long, Long, String) -> Unit = LocalTaskDetailsUIEvents.current.let {
       it::onUpdateInstanceNote
    },
    onSetExpand: (Long) -> Unit = LocalTaskDetailsUIEvents.current.let {
        it::onExpandInstance
    },
) {
    var noteText by rememberSaveable { mutableStateOf(instance.notes) }
    Box {
        TextField(
            noteText,
            {
                noteText = it
                onNoteEdit(instance.task_id, instance.uid, noteText)
            },
            modifier
                .fillMaxWidth(),
            textStyle = InstanceNotesTextFieldTextStyle
        )
        Text(
            "↑",
            Modifier
                .align(Alignment.TopEnd)
                .padding(top = 10.dp, end = 2.dp)
                .clickable { onSetExpand(instance.uid) },
            style = InstanceNotesUpArrowTextStyle
        )
    }
}
