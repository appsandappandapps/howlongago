package app.howlongago.ui.comps

import android.util.Log
import androidx.activity.compose.BackHandler
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalSavedStateRegistryOwner
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import app.howlongago.LifecycleOnResumeObserver
import app.howlongago.StateSaver
import app.howlongago.taskdetails.TaskDetailsUIEvents
import app.howlongago.taskdetails.TaskDetailsUIState
import app.howlongago.taskdetails.TaskDetailsViewModel
import kotlinx.coroutines.flow.MutableStateFlow

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun TaskDetailScreen(
  modifier: Modifier = Modifier,
  taskId: Long = LocalNavigationUIState.current.taskIdForDetailsPage,
  currentPage: String = LocalNavigationUIState.current.page,
  onGoBack: () -> Unit = LocalNavigationUIEvents.current.let {
    it::goBackoListPage
  },
  onForceUIUpdate: () -> Unit = LocalTaskDetailsUIEvents.current.let {
    it::onForceUIUpdate
  },
) {

  if(currentPage == "detail") BackHandler { onGoBack() }
  LifecycleOnResumeObserver { onForceUIUpdate() }

  BoxWithConstraints {
    // Animation it into place
    val transition = updateTransition(currentPage == "list")
    val pageTransitionAlpha by transition.animateFloat(
      transitionSpec = {
        spring(
          dampingRatio = Spring.DampingRatioNoBouncy,
          stiffness = Spring.StiffnessLow
        )
      }
    ) {
      if (currentPage == "detail") 1.0f
      else 0.0f
    }
    val restingPlace = with(LocalDensity.current) {
      (maxWidth.toPx() / 5).let { maxWidth.toPx() - it }
    }
    val detailsTranslationX by transition.animateFloat(
      transitionSpec = {
        spring(
          dampingRatio = Spring.DampingRatioNoBouncy,
          stiffness = Spring.StiffnessMediumLow
        )
      }
    ) {
      if (currentPage == "detail") 0f
      else restingPlace
    }
    // The view
    TaskDetail(
      taskId,
      modifier
        .graphicsLayer {
          translationX =
            if(detailsTranslationX == restingPlace)
              -5000f
            else
              detailsTranslationX
        }
        .alpha(pageTransitionAlpha)
    )
  }

}
