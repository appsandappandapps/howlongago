package app.howlongago.ui.comps.task

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.Button
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.contentColorFor
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.howlongago.ComposeFn
import app.howlongago.KeyboardOptionsDone
import app.howlongago.background
import app.howlongago.button
import app.howlongago.h1
import app.howlongago.onBackground
import app.howlongago.ui.comps.LocalTaskDetailsUIEvents
import app.howlongago.ui.comps.LocalTaskDetailsUIState

// A bar with an editable task
// name and a delete button
@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun TaskDetailTopBar(
  modifier: Modifier = Modifier,
  taskId: Long,
  taskName: String = LocalTaskDetailsUIState.current.currentTaskName,
  onUpdateTaskName: (Long, String) -> Unit = LocalTaskDetailsUIEvents.current.let {
    it::onUpdateTaskName
  },
  onInsertInstance: (Long) -> Unit = LocalTaskDetailsUIEvents.current.let {
    it::onInsertInstanceAndExpandIt
  },
) {
  val keyboardController = LocalSoftwareKeyboardController.current
  Column {
    Row(
      modifier.fillMaxWidth(),
      verticalAlignment = Alignment.CenterVertically
    ) {
      BasicTextField(
        taskName,
        { onUpdateTaskName(taskId, it) },
        modifier = Modifier
          .weight(2f)
          .padding(end = 2.dp),
        textStyle = h1.plus(
          TextStyle(
            fontSize = 24.sp,
            color = contentColorFor(background),
          ),
        ),
        cursorBrush = SolidColor(onBackground),
        keyboardOptions = KeyboardOptionsDone,
        keyboardActions = KeyboardActions {
          keyboardController?.hide()
        },
      )
      TaskDeleteButton(taskId)
    }
    Surface(
      Modifier
        .fillMaxWidth()
        .padding(top = 20.dp),
      elevation = 2.dp,
    ) {
      Button({
        onInsertInstance(taskId)
      }) {
        Text(
          "I JUST DID IT",
          style = button.copy(
            fontWeight = FontWeight.SemiBold,
          ),
        )
      }
    }
  }
}