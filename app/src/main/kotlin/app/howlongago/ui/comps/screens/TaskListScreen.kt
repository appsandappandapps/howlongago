package app.howlongago.ui.comps

import android.os.Environment
import android.util.Log
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalSavedStateRegistryOwner
import androidx.compose.ui.unit.dp
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import app.howlongago.LifecycleOnResumeObserver
import app.howlongago.StateSaver
import app.howlongago.background
import app.howlongago.clickableNoRipple
import app.howlongago.onSurface
import app.howlongago.primary
import app.howlongago.surface
import app.howlongago.tasklist.TasksUIEvents
import app.howlongago.tasklist.TasksUIState
import app.howlongago.tasklist.TasksViewModel
import app.howlongago.tasklist.model.TaskWithInstanceDateUI
import app.howlongago.ui.comps.settings.TasksTopBar
import app.howlongago.ui.comps.task.TaskInputField
import app.howlongago.ui.comps.tasklist.TaskList
import kotlinx.coroutines.flow.MutableStateFlow

val LocalPositionUnderAddButton = compositionLocalOf { mutableStateOf(0F) }

@Composable
fun TaskListScreen(
  modifier: Modifier = Modifier,
  currentPage: String = LocalNavigationUIState.current.page,
  tasksState: TasksUIState = LocalTaskListUIState.current,
  onExpandToggle: () -> Unit = LocalTaskListUIEvents.current.let { it::onToggleExpandTextEntry },
) {

  BoxWithConstraints {
    // Animate into place
    val transition = updateTransition(currentPage == "list")
    val pageTransitionAlpha by transition.animateFloat(
      transitionSpec = {
        spring(
          dampingRatio = Spring.DampingRatioNoBouncy,
          stiffness = Spring.StiffnessLow
        )
      }
    ) {
      if (currentPage == "list") 1.0f
      else 0.0f
    }
    val restingPlace = with(LocalDensity.current) { (maxWidth.toPx() / 2) * -1 }
    val listTranslationX by transition.animateFloat(
      transitionSpec = {
        spring(
          dampingRatio = Spring.DampingRatioNoBouncy,
          stiffness = Spring.StiffnessMediumLow
        )
      }
    ) {
      if (currentPage == "list") 0f
      else restingPlace
    }
    // The view
    Column(
      Modifier
        .graphicsLayer {
          translationX =
            if (listTranslationX == restingPlace)
              -5000f
            else
              listTranslationX
        }
        .alpha(pageTransitionAlpha)
        .fillMaxSize()
        .padding(start = 14.dp, end = 14.dp, top = 14.dp),
      verticalArrangement = Arrangement.Center
    ) {
      TasksTopBar()
      TaskList(
        modifier.weight(2f),
      )
    }
  }


  BoxWithConstraints {

    val configuration = LocalConfiguration.current
    val height = configuration.screenHeightDp / 3F
    val localPosition = tasksState.ypos
    val inputTranslationX by animateFloatAsState(
      if (tasksState.expandTextEntry) localPosition
      else -1F * with(LocalDensity.current) { height.dp.toPx() },
      animationSpec = spring(
        dampingRatio = Spring.DampingRatioNoBouncy,
        stiffness = Spring.StiffnessMediumLow
      )
    )
    val dp = with(LocalDensity.current) {
      inputTranslationX.toDp() + 10.dp
    }
    Box(
      Modifier
        .fillMaxSize()
    ) {
      if(tasksState.expandTextEntry)
        Box(
          Modifier
            .fillMaxSize()
            .clickableNoRipple { onExpandToggle() }
        ) {}
      Surface(
        Modifier
          .height(height.dp - 14.dp)
          .fillMaxWidth()
          .offset(y = dp)
          .padding(start = 14.dp, end = 14.dp)
          .border(4.dp, primary, RoundedCornerShape(5.dp)),
        elevation = 1.dp
      ) {
        TaskInputField(Modifier.fillMaxSize())
      }
    }
  }

}