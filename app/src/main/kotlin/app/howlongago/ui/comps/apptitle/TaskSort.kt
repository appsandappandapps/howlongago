package app.howlongago.ui.comps.settings

import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.material.DropdownMenu
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import app.howlongago.background
import app.howlongago.onSurface
import app.howlongago.surface
import app.howlongago.ui.comps.LocalTaskListUIEvents
import app.howlongago.ui.comps.LocalTaskListUIState
import app.howlongago.usecases.datasources.SortEnum

// User can choose how to
// sort the tasks
@Composable
fun  TaskSort(
  modifier: Modifier = Modifier,
  sortItemClick: (SortEnum) -> Unit = LocalTaskListUIEvents.current.let { it::onClickSortItem },
  setSortMenuExpanded: (Boolean) -> Unit = LocalTaskListUIEvents.current.let { it::onClickSortMenuExpand },
  sortType: SortEnum = LocalTaskListUIState.current.sortType,
  sortMenuExpanded: Boolean = LocalTaskListUIState.current.isSortMenuExpanded,
) {
  Column(modifier) {
    Icon(
      Icons.Filled.KeyboardArrowDown,
      "filter",
      Modifier.clickable(!sortMenuExpanded) { setSortMenuExpanded(true) }
    )
    DropdownMenu(
      sortMenuExpanded,
      { setSortMenuExpanded(false) },
      Modifier.border(2.dp, background),
    ) {
      SortMenuItem(sortType, SortEnum.DATE_DESC, sortItemClick) {
        Text(
          "Most recent",
          color = if (it) surface
          else onSurface
        )
      }
      SortMenuItem(sortType, SortEnum.DATE_ASC, sortItemClick) {
        Text(
          "Least recent",
          color = if (it) surface
          else onSurface
        )
      }
      SortMenuItem(sortType, SortEnum.NAME_ASC, sortItemClick) {
        Text(
          "Name, abc",
          color = if (it) surface
          else onSurface
        )
      }
      SortMenuItem(sortType, SortEnum.NAME_DESC, sortItemClick) {
        Text(
          "Name, zyx",
          color = if (it) surface
          else onSurface
        )
      }
    }
  }
}