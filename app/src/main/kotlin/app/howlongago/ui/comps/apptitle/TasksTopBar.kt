package app.howlongago.ui.comps.settings

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import app.howlongago.ui.comps.LocalNavigationUIEvents
import app.howlongago.ui.comps.TaskInputButton
import app.howlongago.ui.comps.task.AppHeaderFontStyle

// Main page task bar
@Composable
fun TasksTopBar(
  onAppButtonClick: () -> Unit = LocalNavigationUIEvents.current.let { it::onToggleSettings },
) {
  Box(
    Modifier
      .padding(bottom = 20.dp)
      .fillMaxWidth(),
  ) {
    Column {
      Row {
        TaskInputButton()
        Spacer(Modifier.weight(2F))
        Icon(
          Icons.Filled.Settings, "app menu",
          Modifier
            .padding(top = 2.dp)
            .clickable { onAppButtonClick() }
        )
      }
      Row(
        Modifier
          .padding(end = 10.dp, top = 20.dp),
        verticalAlignment = Alignment.CenterVertically,
      ) {
        Text(
          "How Long Ago",
          style = AppHeaderFontStyle,
          softWrap = false,
        )
        TaskSort(Modifier.padding(top = 8.dp))
      }
    }
  }
}
