package app.howlongago.ui.comps.instance

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.relocation.BringIntoViewRequester
import androidx.compose.foundation.relocation.bringIntoViewRequester
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import app.howlongago.*
import app.howlongago.taskdetails.model.InstanceUI
import app.howlongago.ui.comps.LocalTaskDetailsUIEvents
import app.howlongago.ui.comps.task.InstanceDateAgoTextStyle
import app.howlongago.ui.comps.task.InstanceInbetweenTextStyle
import app.howlongago.ui.comps.task.InstanceNotesLabelTextStyle
import kotlinx.coroutines.launch

// The stuff that's in the instance row
// That expands into an edit row
@OptIn(
  ExperimentalAnimationApi::class,
  ExperimentalFoundationApi::class
)
@Composable
fun InstanceRowExpandable(
  instance: InstanceUI,
  expandToEdit: Boolean = false,
  onUpdateDate: (Long, Long, Long) -> Unit = LocalTaskDetailsUIEvents.current.let {
    it::onUpdateInstanceDate
  },
  onDeleteInstance: (Long, Long) -> Unit = LocalTaskDetailsUIEvents.current.let {
    it::onDeleteInstance
  },
  onExpandInstanceClick: (Long) -> Unit = LocalTaskDetailsUIEvents.current.let {
    it::onExpandInstance
  },
) {
  Column(
    Modifier
      .padding(10.dp)
      .fillMaxWidth()
  ) {
    Text(instance.friendlyDateAgo,
      Modifier
        .fillMaxWidth()
        .clickableNoRipple {
          onExpandInstanceClick(instance.uid)
        },
      style = InstanceDateAgoTextStyle,
    )
    // Show the unexpanded notes
    AnimatedVisibility(!expandToEdit) {
      Text(
        instance.notesLabel,
        Modifier
          .padding(bottom = 12.dp, top = 10.dp)
          .fillMaxWidth()
          .clickable {
            onExpandInstanceClick(instance.uid)
          },
        style = InstanceNotesLabelTextStyle(italic = instance.notes.isBlank())
      )
    }
    // Show the expanded notes
    AnimatedVisibility(expandToEdit) {
      InstanceNoteEdit(
        Modifier
          .padding(top = 10.dp, bottom = 10.dp),
        instance = instance,
      )
    }
    InstanceDateTime(
      Modifier.clickable { onExpandInstanceClick(instance.uid) },
      instance,
      expandToEdit
    ) {
      onUpdateDate(instance.task_id, instance.uid, it.time)
    }
    // Show the delete button if expanded
    AnimatedVisibility(expandToEdit) {
      Row {
        Spacer(Modifier.weight(2f))
        InstanceDeleteButton() {
          onDeleteInstance(instance.task_id, instance.uid)
        }
      }
    }
  }
}