package app.howlongago.ui.comps.task

import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.sp
import app.howlongago.body1
import app.howlongago.onBackground
import app.howlongago.onSurface
import app.howlongago.surface

val AppHeaderFontStyle
  @Composable get() = body1
    .copy(
      fontSize = 26.sp,
      color = onBackground.copy(alpha = 0.95f),
    )

val SettingsAbout
  @Composable get() = body1
    .copy(
      fontSize = 14.sp,
    )

val SettingsTheme
  @Composable get() = body1
    .copy(
      fontSize = 14.sp,
    )

val SettingsStoreLinks
  @Composable get() = body1
    .copy(
      fontSize = 14.sp,
    )

val TaskInputFontStyle
  @Composable get() = body1
    .copy(
      fontSize = 30.sp,
      color = onBackground.copy(alpha = 0.9f),
    )

val InstanceInbetweenTextStyle
  @Composable get() = body1
    .copy(
      fontSize = 11.sp,
      fontStyle = FontStyle.Italic,
      color = onSurface.copy(alpha = 0.4f),
    )

@Composable
fun InstanceNotesLabelTextStyle(italic: Boolean) = body1
  .copy(
    color = onSurface.copy(alpha = 0.6f),
    fontStyle = if(italic) FontStyle.Italic else FontStyle.Normal,
    fontSize = 14.sp,
  )

val InstanceDateAgoTextStyle
  @Composable get() = body1
    .copy(
      color = onSurface.copy(alpha = 0.9f),
      fontSize = 16.sp,
    )

val InstanceNotesTextFieldTextStyle
  @Composable get() =
    TextStyle(
      fontSize = 14.sp,
      color = onBackground
    )

val InstanceNotesUpArrowTextStyle
  @Composable get() =
    TextStyle(
      fontSize = 26.sp,
      color = Color.LightGray,
    )

val SettingsBackupPointerTextStyle
  @Composable get() =
    TextStyle(
      fontSize = 18.sp,
      color = onBackground.copy(alpha = 0.7f),
    )

val SettingsBackupImportPointerTextStyle
  @Composable get() =
    TextStyle(
      fontSize = 18.sp,
      color = onBackground.copy(alpha = 0.7f),
    )


val SettingsBackupImportTextStyle
  @Composable get() =
    TextStyle(
      fontSize = 14.sp,
      color = onBackground.copy(alpha = 0.7f),
    )

val TaskRowTextStyle
  @Composable get() =
    TextStyle(
      color = onSurface.copy(alpha = 0.9f),
      fontSize = 20.sp,
    )

val TaskRowDateTextStyle
  @Composable get() =
    TextStyle(
      color = onSurface.copy(alpha = 0.6f),
      fontSize = 14.sp,
    )

