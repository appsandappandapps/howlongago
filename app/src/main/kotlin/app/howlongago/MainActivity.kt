package app.howlongago

import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.*
import androidx.compose.ui.unit.dp
import app.howlongago.ui.comps.HowLongAgo
import app.howlongago.ui.theme.HowLongAgoTheme

val LocalAppCompatActivity = compositionLocalOf { AppCompatActivity() }

class MainActivity : AppCompatActivity() {

  companion object {
    val SAVE_DATABASE_REQUEST: Int = 100
    val IMPORT_DATABASE_REQUEST: Int = 200
  }

  override fun onCreate(state: Bundle?) {
    super.onCreate(state)

    requestWindowFeature(Window.FEATURE_NO_TITLE)
    supportActionBar?.hide()

    setContent {

      CompositionLocalProvider(
        LocalAppCompatActivity provides this,
      ) {
        //val taskId = intent.getLongExtra("${HowLongAgoWidget.TaskIdParam}", -1)
        val darkMode = Preferences.darkTheme.collectAsState()
        HowLongAgoTheme(darkMode.value) {
          HowLongAgo()
        }
      }
    }
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    if(requestCode == MainActivity.SAVE_DATABASE_REQUEST) {
      when (resultCode) {
        RESULT_OK -> if (data != null && data.data != null) {
          saveDatabaseWithUI(data.data!!)
        }
        RESULT_CANCELED -> {}
      }
    } else if(requestCode == MainActivity.IMPORT_DATABASE_REQUEST) {
      when (resultCode) {
        RESULT_OK -> if (data != null && data.data != null) {
          importDatabaseWithUI(data.data!!)
        }
        RESULT_CANCELED -> {}
      }
    }
  }

}