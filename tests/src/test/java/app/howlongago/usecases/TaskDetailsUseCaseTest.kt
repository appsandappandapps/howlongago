package app.howlongago.usecases

import app.howlongago.db.Instance
import app.howlongago.db.Task
import app.howlongago.db.TaskWithInstance
import app.howlongago.usecases.TaskDetailsUseCase
import app.howlongago.usecases.TasksSortedUseCase
import kotlinx.coroutines.test.runTest
import org.junit.Test
import org.junit.Assert.*

import app.howlongago.usecases.datasources.SortEnum
import app.howlongago.usecases.repositories.InstancesRepository
import app.howlongago.usecases.repositories.TasksRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before

class TaskDetailsUseCaseTest {

  val testDispatcher = StandardTestDispatcher()

  @MockK(relaxed = true)
  lateinit var tasksRepo: TasksRepository

  @MockK(relaxed = true)
  lateinit var instancesRepo: InstancesRepository

  @MockK(relaxed = true)
  lateinit var sortedTaskUseCase: TasksSortedUseCase

  @Before fun setUp() {
    MockKAnnotations.init(this)
    Dispatchers.setMain(testDispatcher)
  }

  @After fun tearDown() {
    Dispatchers.resetMain()
  }

  @Test
  fun `test setSelectedTask()`() = runTest {
    val task = Task(0, "taskName")
    val instances: List<Instance> = listOf(Instance(100, 100, 100, "", null))
    coEvery { instancesRepo.getAll(1) } returns instances
    coEvery { tasksRepo.get(1) } returns task

    val tasksUseCase = TaskDetailsUseCase(tasksRepo, instancesRepo, sortedTaskUseCase)
    tasksUseCase.setSelectedTask(taskId = 1)
    val returnedInstances = tasksUseCase.instances.first()
    val returnedSelectedTask = tasksUseCase.selectedTask.first()

    assertEquals(instances, returnedInstances)
    assertEquals(task, returnedSelectedTask)
  }

  @Test
  fun `test insertInstance()`() = runTest {
    val taskId = 2L
    val instances: List<Instance> = listOf(Instance(100, 100, 100, "", null))
    coEvery { instancesRepo.insert(taskId, any()) } returns 100
    coEvery { instancesRepo.getAll(taskId) } returns instances

    val tasksUseCase = TaskDetailsUseCase(tasksRepo, instancesRepo, sortedTaskUseCase)
    val returnedInstanceId = tasksUseCase.insertInstance(taskId)
    val returnedInstances = tasksUseCase.instances.first()

    coVerify(exactly = 1) { sortedTaskUseCase.getAllSortedIntoMutableState() }
    assertEquals(instances, returnedInstances)
    assertEquals(100, returnedInstanceId)
  }

  @Test
  fun `test deleteInstance()`() = runTest {
    val instances: List<Instance> = listOf(Instance(100, 100, 100, "", null))
    coEvery { instancesRepo.getAll(2) } returns instances

    val tasksUseCase = TaskDetailsUseCase(tasksRepo, instancesRepo, sortedTaskUseCase)
    tasksUseCase.deleteInstance(taskId = 2, instanceId = 1)
    val returnedInstances = tasksUseCase.instances.first()

    coVerify(exactly = 1) { instancesRepo.delete(1) }
    coVerify(exactly = 1) { sortedTaskUseCase.getAllSortedIntoMutableState() }
    assertEquals(instances, returnedInstances)
  }

  @Test
  fun `test deleteTask()`() = runTest {
    TaskDetailsUseCase(tasksRepo, instancesRepo, sortedTaskUseCase)
      .deleteTask(1)

    coVerify(exactly = 1) { tasksRepo.deleteWithInstances(1) }
    coVerify(exactly = 1) { sortedTaskUseCase.getAllSortedIntoMutableState() }
  }

  @Test
  fun `test updateInstanceDate()`() = runTest {
    val instances: List<Instance> = listOf(Instance(100, 100, 100, "", null))
    coEvery { instancesRepo.getAll(2) } returns instances

    val tasksUseCase = TaskDetailsUseCase(tasksRepo, instancesRepo, sortedTaskUseCase)
    tasksUseCase.updateInstanceDate(taskId = 2, instanceId = 1, 100L)
    val returnedInstances = tasksUseCase.instances.first()

    coVerify(exactly = 1) { instancesRepo.updateDate(1, 100L) }
    coVerify(exactly = 1) { sortedTaskUseCase.getAllSortedIntoMutableState() }
    assertEquals(instances, returnedInstances)
  }

  @Test
  fun `test updateInstanceNotes()`() = runTest {
    val instances: List<Instance> = listOf(Instance(100, 100, 100, "", null))
    coEvery { instancesRepo.getAll(2) } returns instances
    coEvery { instancesRepo.updateNoteDelayed(1, "new note", any()) } answers {
      launch { (it.invocation.args[2] as (suspend () -> Unit))() }
    }

    val tasksUseCase = TaskDetailsUseCase(tasksRepo, instancesRepo, sortedTaskUseCase)
    tasksUseCase.updateInstanceNote(taskId = 2, instanceId = 1, note = "new note")
    advanceUntilIdle()
    val returnedInstances = tasksUseCase.instances.first()

    coVerify(exactly = 1) { instancesRepo.updateNoteDelayed(1, "new note", any()) }
    assertEquals(instances, returnedInstances)
  }

  @Test
  fun `test updateTaskName()`() = runTest {
    val instances: List<Instance> = listOf(Instance(100, 100, 100, "", null))
    coEvery { instancesRepo.getAll(2) } returns instances
    coEvery { tasksRepo.updateNameDelayed(2, "new name", any()) } answers {
      launch { (it.invocation.args[2] as (suspend () -> Unit))() }
    }

    val tasksUseCase = TaskDetailsUseCase(tasksRepo, instancesRepo, sortedTaskUseCase)
    tasksUseCase.updateTaskName(uid = 2, name = "new name")
    advanceUntilIdle()

    coVerify(exactly = 1) { tasksRepo.updateNameDelayed(2, "new name", any()) }
    coVerify(exactly = 1) { sortedTaskUseCase.getAllSortedIntoMutableState() }
  }

}
