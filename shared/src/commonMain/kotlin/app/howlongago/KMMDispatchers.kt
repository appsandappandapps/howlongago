package app.howlongago

import app.howlongago.db.Instance
import app.howlongago.db.TaskWithInstance
import app.howlongago.db.TasksAndInstancesQueries
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

val UIDispatcher: CoroutineDispatcher = Dispatchers.Main
val UIImmediateDispatcher: CoroutineDispatcher = Dispatchers.Main.immediate
val DefaultDispatcher: CoroutineDispatcher = Dispatchers.Default
expect val IODispatcher: CoroutineDispatcher
