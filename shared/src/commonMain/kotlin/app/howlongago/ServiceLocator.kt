package app.howlongago

import app.howlongago.usecases.BackupImportUseCase
import app.howlongago.usecases.TaskDetailsUseCase
import app.howlongago.usecases.repositories.TasksRepository
import app.howlongago.usecases.TasksSortedUseCase

expect object ServiceLocator {
  var sortedTasksUseCase: TasksSortedUseCase?
  var taskDetailsUseCase: TaskDetailsUseCase?
  var backupImportUseCase: BackupImportUseCase?
  fun init(context: AppContext)
}