package app.howlongago

import kotlinx.coroutines.delay
import kotlinx.datetime.Clock
import kotlin.time.Duration
import kotlin.time.ExperimentalTime

@OptIn(ExperimentalTime::class)
fun friendlyDateAgo(
  smallest: Long,
  biggest: Long = Clock.System.now().toEpochMilliseconds(),
  showMinutes: Boolean = true,
  postFix:String = "ago"
): String {
  val diffInMillis = biggest - smallest
  var duration = Duration.milliseconds(diffInMillis)

  val days = duration.inWholeDays.toInt()
  duration = duration.minus(Duration.days(days))
  val hours = duration.inWholeHours.toInt()
  duration = duration.minus(Duration.hours(hours))
  val minutes = duration.inWholeMinutes.toInt()

  val m = if(minutes == 1) "minute" else "minutes"
  val h = if(hours == 1) "hour" else "hours"
  val d = if(days == 1) "day" else "days"
  val daysStr = if(days > 0) "${days} ${d} " else ""
  val hoursStr = if(hours > 0) "${hours} ${h} " else ""
  var minsStr = if(showMinutes && minutes > 0) "${minutes} ${m} " else ""
  minsStr = if(!showMinutes && minutes > 0 && hoursStr.isEmpty() && daysStr.isEmpty() ) "${minutes} ${m} " else minsStr
  val str = "${daysStr}${hoursStr}${minsStr}"
  return if(str.trim().isEmpty() && postFix == "ago") "Just now"
  else if(str.trim().isEmpty() && postFix == "inbetween") "a few moments inbetween"
  else str + postFix
}

// So we can mock it...
suspend fun delayLong(time: Long) = delay(time)
