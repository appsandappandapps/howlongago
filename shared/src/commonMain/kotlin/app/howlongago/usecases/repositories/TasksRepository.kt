package app.howlongago.usecases.repositories

import app.howlongago.IODispatcher
import app.howlongago.TaskBackup
import app.howlongago.TaskWithInstancesBackup
import app.howlongago.db.Task
import app.howlongago.db.TaskWithInstance
import app.howlongago.db.TasksAndInstancesQueries
import app.howlongago.db.TasksBackup
import app.howlongago.delayLong
import com.squareup.sqldelight.TransactionWithReturn
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * - Modifies tasks and instances
 * - Delayed update methods for update task name and instance notes
 * - Imports the databse
 *
 * TODO:
 * - We're not testing if delayed is called
 * - We're not testing if a suspending lambda is called
 */


class TasksRepository(
  private val db: TasksAndInstancesQueries,
  private val ioScope: CoroutineScope = CoroutineScope(IODispatcher),
  private var updateTaskNameDelayedJob: Job? = null,
) {

  public suspend fun get(taskId: Long): Task {
    return db.getTask(taskId).executeAsOne()
  }

  public suspend fun getAllDateDesc(): List<TaskWithInstance> {
    return db.getTasksByInstanceDateDesc().executeAsList()
  }

  public suspend fun getAllDateAsc(): List<TaskWithInstance> {
    return db.getTasksByInstanceDateAsc().executeAsList()
  }

  public suspend fun getAllNameDesc(): List<TaskWithInstance> {
    return db.getTasksByNameDesc().executeAsList()
  }

  public suspend fun getAllNameAsc(): List<TaskWithInstance> {
    return db.getTasksByNameAsc().executeAsList()
  }

  public suspend fun insert(taskName: String): Long {
    return db.transactionWithResult {
      db.insertTask(null, taskName.trim())
      db.latestInstance().executeAsOne()
    }
  }

  public suspend fun updateNameDelayed(uid: Long, name: String, onComplete: suspend () -> Unit) {
    updateTaskNameDelayedJob?.cancel()
    updateTaskNameDelayedJob = ioScope.launch {
      delayLong(250)
      db.updateTask(name.trim(), uid)
      onComplete()
    }
  }

  public suspend fun deleteWithInstances(uid: Long) {
    db.deleteTask(uid)
    db.deleteInstanceByTask(uid)
  }

  public suspend fun tasksBackup(): List<TasksBackup> {
    return db.tasksBackup().executeAsList()
  }

}