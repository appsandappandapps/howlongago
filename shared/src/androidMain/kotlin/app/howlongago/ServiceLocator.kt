package app.howlongago

import app.howlongago.usecases.datasources.TaskSortingDatasource
import app.howlongago.db.TasksAndInstancesQueries
import app.howlongago.usecases.BackupImportUseCase
import app.howlongago.usecases.repositories.TasksSortingRepository
import app.howlongago.usecases.repositories.TasksRepository
import app.howlongago.usecases.TasksSortedUseCase
import app.howlongago.usecases.TaskDetailsUseCase
import app.howlongago.usecases.repositories.InstancesRepository

actual object ServiceLocator {

  actual var sortedTasksUseCase: TasksSortedUseCase? = null
  actual var taskDetailsUseCase: TaskDetailsUseCase? = null
  actual var backupImportUseCase: BackupImportUseCase? = null

  actual fun init(context: AppContext) {
    val tasksDb: TasksAndInstancesQueries = tasksDbInit(context)
    val taskSortingDatasource = TaskSortingDatasource(context)
    val taskSortingRepo = TasksSortingRepository(taskSortingDatasource)
    val instancesRepo = InstancesRepository(tasksDb)
    val taskRepository = TasksRepository(db = tasksDb)
    this.sortedTasksUseCase = TasksSortedUseCase(taskRepository, taskSortingRepo)
    this.taskDetailsUseCase = TaskDetailsUseCase(taskRepository, instancesRepo, sortedTasksUseCase!!)
    this.backupImportUseCase = BackupImportUseCase(taskRepository, instancesRepo, sortedTasksUseCase!!)
  }

}